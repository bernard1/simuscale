# view

The tool `view` can be used to build visualize the simulation results from the 
main simulation program `simuscale`. `view` is distributed with Simuscale, and
is automatically built with the make command. It can also be independently built
with

    make view

