# lineagetree

The tool `lineagetree` can be used to build visualize the simulation results from the 
main simulation program `simuscale`. `lineagetree` is distributed with Simuscale, and
is automatically built with the make command. It can also be independently built
with

    make lineagetree

Simuscale produces the lineage tree in two formats: [Newick](https://en.wikipedia.org/wiki/Newick_format)
and trivial graph format [TGF](https://en.wikipedia.org/wiki/Trivial_Graph_Format). 


The Newick format is a text-based format encoding a binary tree that records the
whole cell lineage.  Unlike many graph formats, the Newick format provides a way
to specify the length of each edge with the syntax `node:length`.  Inner
nodes (non-leaf, non-root nodes) represent cell division, with one branch for
the mother cell and one branch for the daughter cell. Leaf nodes represent the
end of existence of a cell (either death or end of simulation).  A cell
division is recorded as `(descendants)cellID>motherID:ageOfMother` where
`descendants` is the tree of the descendants of cell with ID `cellID`, 
a root at `cellID`. Cell deaths are
recorded as tree leaves: `cellID:ageAtDeath`. Cells that are alive at the
end of the simulation are also recorded as leaves: `cellID:ageAtT`. The complete
tree is a comma separated list of subtrees enclosed by parentheses and followed by
`cellID>motherID:ageOfMother`.  Because cells initially present at `t=0`
do not have ancestors, a "root" cell with ID=0 is added to the tree to make
it rooted. Therefore the Newick tree always ends with

    ...>root:0.00)2>root:0.00)1>root:0.00;

## Usage

To produce a SVG file of the tree `treelist.tgf`, use 

    lineagetree treelist.tgf

The output is a file `treelist.svg`, which can be viewed in a web browser.
The converting tool `rsvg-convert` can be used to generate a PNG file

    rsvg-convert treelist.svg >treelist.png`

### Use with python Phylo from Bio

From the [Phylo documentation](https://biopython.org/wiki/Phylo):

    from Bio import Phylo
    tree = Phylo.read("newick.tree", "newick")
    Phylo.draw(tree) # draw a linear tree

### Use with ETE 

From the [ETE documentation](http://etetoolkit.org/docs/latest/tutorial/tutorial_drawing.html#interactive-visualization-of-trees)

    from ete3 import Tree, TreeStyle
    t = Tree("newick.tree",format=1)
    t.show() # linear tree style
    circular_style = TreeStyle()
    circular_style.mode = "c"
    t.show(tree_style=circular_style)

## Options

Linear or radial (default) trees can be produced, with the option 
`-t linear` or `-t radial`.


