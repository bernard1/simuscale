# simuscale

The tool `simuscale` is the main simulation program to perform cell population simulations.
It is automatically built with the make command. It can also be independently built
with

    make simuscale

## Parameter file


Keyword             Parameters                         Description 
------------        ----------------                   ---------------
`PRNG_SEED`         (integer)`n` or `AUTO`             pseudo-random generator seed `n` or  
                                                       automatically generated (`AUTO`) 
`MAXTIME`           (float)`T`                         maximal time `T` for the simulations 
`DT`                (float)`dt`                        core simulator time step 
`BACKUP_DT`         (float)`bdt`                       Backup time step 
`ADD_POPULATION`    (int)`N` ...                       cell number 
                    (CellType)`type` ...               _type_ in `CellType.values.in`  
                    (CellFormalism)`p` ...             plugin name (`classKW_` in plugin file) 
                    (MoveBehaviour)`m` ...             `m` in {`IMMOBILE`, `MOBILE`, `MOTILE`} 
                    (float)`d` ...                     volume doubling time, `d>0` 
                    (float)`v` ...                     minimal initial volume 
                    [(string)_init.file_]              initial positions file 
`R_RATIO`           (float)`r`                         ratio between internal and external radii 
`SIGNAL`            (InterCellSignal)`s`               record signal `s` to trajectory file  
`WORLDSIZE`         (float)`x` (float)`y` (float)`z`   size of the computational domain 
                                                       (default: 40 40 40) 
`USECONTACTAREA`    0 or 1                             cell signals weighed by the cell-cell  
                                                       contact area (1) or not (0) 
`WRITEORIENTATION`  0 or 1                             record orientation coordinates to trajectory file
`LJMAXFORCE`        (float)f                           max force in cell-cell contact (default: 0.5)
`LJEPSILON`         (float)eps                         scaling constant in cell-cell contact (default: 0.002)
