// ****************************************************************************
//
//              SiMuScale - Multi-scale simulation framework
//
// ****************************************************************************
//
// Copyright: See the AUTHORS file provided with the package
// E-mail: simuscale-contact@lists.gforge.inria.fr
// Original Authors : Samuel Bernard, Carole Knibbe, David Parsons
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ****************************************************************************

// ============================================================================
//                                   Includes
// ============================================================================
#include <iostream>
#include "Cell_Boilerplate.h"
#include "Simulation.h"


using std::cout;

// ============================================================================
//                       Definition of static attributes
// ============================================================================
constexpr CellFormalism Cell_Boilerplate::classId_;
constexpr char Cell_Boilerplate::classKW_[];

/* Use diffusive signalling 
 * comment out if not using */
#define DIFFUSIVE_EXAMPLE

/** MACRO DUMPNN: dump name and non-numerical value of variable _var_
 * evil macro -- perhaps there is a better solution */
#define DUMPNN(var) do  {std::cout << ",\n    " << "\"" #var "\": " << "\"" << (var) << "\"";} while(false)

/** MACRO DUMP: dump name and numerical value of variable _var_
 * evil macro -- perhaps there is a better solution */
#define DUMP(var) do  {std::cout << ",\n    " << "\"" #var "\": " << (var);} while(false)

// ============================================================================
//                                Constructors
// ============================================================================
/**
 * Create a new object with the provided constitutive elements and values
 */
Cell_Boilerplate::Cell_Boilerplate(CellType cellType,
                   const MoveBehaviour& move_behaviour,
                   const Coordinates<double>& pos,
                   double initial_volume,
                   double volume_min,
                   double doubling_time) :
    Cell(cellType, move_behaviour,
         pos, CellSize(initial_volume, volume_min),
         doubling_time) {
}

/** 
 * Create a new object from an existing one 
 */
Cell_Boilerplate::Cell_Boilerplate(const Cell_Boilerplate &model):
  Cell(model) {

  // <TODO> Reset any variable that need to be updated from mother cell _model_
  age_ = 0.0;
  // </TODO>

}

/**
 * Restore an object that was backed-up in backup_file
 */
Cell_Boilerplate::Cell_Boilerplate(gzFile backup_file) :
    Cell(backup_file) {
  Load(backup_file);
}

// ============================================================================
//                                 Destructor
// ============================================================================
Cell_Boilerplate::~Cell_Boilerplate() = default;

// ============================================================================
//                                   Methods
// ============================================================================
void Cell_Boilerplate::InternalUpdate(const double& dt) {
  // <TODO>
  // Describe what happens inside the cell when taking a time step

  (void)dt; // to avoid compiler warning: unused parameter 'dt'

  // Get the value of signal BP1 from neighbouring cells
  RBP1_ = get_local_signal(InterCellSignal::BP1);

#ifdef DIFFUSIVE_EXAMPLE
  /* Get values of diffusive signal BP1 sent
   * by each cell with a strength gaussian_field_weight, with source location 
   * gaussian_field_source, measured 
   * at locations specified by gaussian_field_targets(InterCellSignal::BP1)
   */
  std::vector<real_type> v = get_diffusive_signal(InterCellSignal::BP1);
  /* The values includes the contribution of the cell itself. Here we remove the
   * contribution the current cell, and add the fisrt component of v to the
   * local signal in RPB1_
   */
  RBP1_ += v[0] - gaussian_field_weight(InterCellSignal::BP1);
  /* The signal variable BP1_ is updated based on the signal measured RBP1_
   */
  BP1_ += dt*( 100.0*BP1_*BP1_/(100.0 + 0.001*RBP1_*RBP1_ + BP1_*BP1_) - 0.25*BP1_ ); 
#else
  /* Local signalling */
  BP1_ += dt*( 100.0*BP1_*BP1_/(100.0 + 10*RBP1_*RBP1_ + BP1_*BP1_) - 0.25*BP1_ ); 
#endif

  /* Cell death
   * Use set_to_dying() and set_to_dead() to set the dying and dead status
   * of the cell. 
   * (By default, there is no unset/clear method. It is assumed that cells
   * transit from alive -> dying -> dead irreversibly.)
   *
   * Death behaviour
   * If is_dead_ is set, then cell becomes dead
   * Dead cells are removed from the simulation.
   * do not call isDead explicitly
   */
  if ( is_dying_ ) {
    set_to_dead();
    return; // it should die immediately
  }

  /* Dying behaviour
   * If is_dying_ is set, then cell becomes dying
   * Dying cell are marked with D in the trajectory file.
   */
  prob_death_ = dt*death_rate_;
  if ( prob_death_ > Alea::random() ) {
    set_to_dying();
    if ( is_dying_ ) {
    }
    return; // if dying, the method should perhaps return   
  }

  /* Cell Growth
   * Grow(dt) increase cell volume with a doubling time set in param.in 
   * Method Grow() can be redefined
   */
  Grow(dt);

  /* Dividing behaviour is queried by the method isDividing
   * do not call isDividing explicitly, it is called by Simulation
   * once per iteration
   */
  age_ += dt;
  prob_div_ = dt*division_rate_;
  
  // </TODO>

}

Cell* Cell_Boilerplate::Divide(void) {
  // Determine how the cell should divide 

  /* Create a new cell and separate it from the mother cell
   * -- This is the default way to create a new cell, modify only if needed
   * -- Edit constructor Cell_Boilerplate::Cell_Boilerplate(const Cell_Boilerplate &model)
   *    to update the state of the new cell
   */
  Cell_Boilerplate* newCell = new Cell_Boilerplate(*this);  // create a new cell based on _this_ cell 

  SeparateDividingCells(this, newCell);                     // move the two cells away from each other
  // end Create a new cell 

  // <TODO>
  // Update the state of the mother cell 
  age_ = 0.0;
  // </TODO>

  return newCell;
}

void Cell_Boilerplate::Save(gzFile backup_file) const {
  // Write my classId
  gzwrite(backup_file, &classId_, sizeof(classId_));  // do not edit

  // Write the generic cell stuff
  Cell::Save(backup_file);                            // do not edit

  // Write the specifics of this class
  // <TODO> Save your specific attributes
  gzwrite(backup_file,&RBP1_,sizeof(RBP1_)); 
  gzwrite(backup_file,&BP1_,sizeof(BP1_));
  gzwrite(backup_file,&age_,sizeof(age_));
  // </TODO>
}

void Cell_Boilerplate::Load(gzFile backup_file) {
  // <TODO> Load your specific attributes 
  gzread(backup_file,&RBP1_,sizeof(RBP1_)); 
  gzread(backup_file,&BP1_,sizeof(BP1_));
  gzread(backup_file,&age_,sizeof(age_));
  // </TODO>
}

void Cell_Boilerplate::Dump() {

  // dump generic cell stuff;
  Cell::Dump();             // do not edit

  // <TODO> Print your specific attributes 
  DUMP(RBP1_);
  DUMP(BP1_);
  DUMP(age_);
  // </TODO> ----------------------------------

  std::cout << "\n  }";     // do not edit
}

/** local_signal
 * Returns _signal_ expression by the cell
 */
double Cell_Boilerplate::local_signal(InterCellSignal signal) const {
  /* <TODO> Determine the amount of <signal> the cell is secreting 
   * example: - cells express the signalling factor BP1
   *          - The signal RBP1 is used as a reporter for RBP1_
   */
  if ( signal == InterCellSignal::BP1 ) {
#ifdef DIFFUSIVE_EXAMPLE
    return BP1_;  // use with diffusive signalling
#else
    return .1;    // use with local cell-cell signalling
#endif
  }
  else if ( signal == InterCellSignal::RBP1 ) {
    return RBP1_;
  }
  else {
    return 0.0;
  }
  // </TODO>
}

bool Cell_Boilerplate::isDividing() const {
  /* <TODO> Should the cell divide now ? 
   * make cells divide at a slow rate, if
   * they have not divided in the past 12h.
   */
  if ( age_ >= 12.0 )
    return prob_div_ > Alea::random();
  else
    return false;
  // </TODO>
}

/** MotileDisplacement
 * Controls the motile behaviour
 * If movement behaviour is not MOTILE, this 
 * method has no effect.
 */
Coordinates<double> Cell_Boilerplate::motility(const double& dt) {
  // <TODO> Motile cell autonomous displacement behaviour
  Coordinates<double> displ { 0.0, 0.0, 0.0 };
  (void)dt; // to avoid compiler warning: unused parameter 'dt'
  return displ;
  // </TODO>
}

/** gaussian_field_weight
 * Returns the strength of a diffusive signal
 * the signal must be marked as DIFFUSIVE in the 
 * params.in file.
 */
double Cell_Boilerplate::gaussian_field_weight(InterCellSignal signal) {
  // <TODO> set the strength of the diffusive signals
  (void)signal; 
  // example: cell expresses diffusive signal BP1
  if ( signal == InterCellSignal::BP1 ) {
    return BP1_;
  }
  else {
    return 0.0;
  }
  // </TODO>
}

/** gaussian_field_source 
 * Returns the coordinates of the diffusive source
 * Defaults to pos() 
 */
const Coordinates<double>& Cell_Boilerplate::gaussian_field_source(InterCellSignal signal) {
  // <TODO> set the position of the source of the diffusive signals
  (void)signal; 
  return pos();  // default: cell position
  // </TODO>

}

/** gaussian_field_targets
 * Returns the coordinates of the diffusive targets (where the cell 
 * measures the diffusive signals)
 * Defaults to pos() and to pos()+orientation()
 * Any number of targets can be defined
 */
std::vector<Coordinates<double>> Cell_Boilerplate::gaussian_field_targets(InterCellSignal signal) {
  // <TODO> set the vector of positions for sensing diffusive signals 
  (void)signal;
  std::vector<Coordinates<double>> targets;
  targets.push_back(pos());                   // default: cell position and
  targets.push_back(pos() + orientation());   // a position along the cell orientation
  return targets;
  // </TODO>
}


// Register this class in Cell
bool Cell_Boilerplate::registered_ =
    Cell::RegisterClass(classId_, classKW_,
                        [](CellType type,
                           const MoveBehaviour& move_behaviour,
                           const Coordinates<double>& pos,
                           double initial_volume,
                           double volume_min,
                           double doubling_time){
                          return static_cast<Cell*>(
                              new Cell_Boilerplate(type, move_behaviour,
                                           pos, initial_volume, volume_min,
                                           doubling_time));
                        },
                        [](gzFile backup_file){
                          return static_cast<Cell*>(new Cell_Boilerplate(backup_file));
                        }
    );
